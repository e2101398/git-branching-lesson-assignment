package fi.vamk.tka.iotclient;

import com.iotticket.api.v1.IOTAPIClient;
import com.iotticket.api.v1.model.*;
import com.iotticket.api.v1.model.Device.DeviceDetails;

public class TicketClient {
    public static void main(String[] args) {
        try {
            registerDevice();
        } catch (Exception ex) {
            System.out.println(ex);
        }

    }

    public static void registerDevice() throws Exception {
        final String userName = "student2020";
        final String pwd = "student2020_Password";
        final IOTAPIClient client = new IOTAPIClient("https://my.iot-ticket.com/api/v1", userName, pwd);

        Device d = new Device();
        d.setName("e1900481");
        d.setManufacturer("Devicemanufacturer");
        d.setType("DeviceType");
        d.getAttributes().add(new DeviceAttribute("EngineSize", "2.4"));
        d.getAttributes().add(new DeviceAttribute("FuelType", "Benzene"));

        DeviceDetails deviceDetails = client.registerDevice(d);
        System.out
                .println("This is the device id that should be used in subsequent calls when sending measurement data: "
                        + deviceDetails.getDeviceId());
    }
}
