# Iot-Client project

## Project uses Wapice IoT Ticket Java client to create a device at IoT Ticket.

## Initially

1. Compile
   javac -cp .;.\IOT-Ticket-0.0.2-SNAPSHOT-jar-with-dependencies.jar TicketClient.java

2. Run
   java -cp .;.\IOT-Ticket-0.0.2-SNAPSHOT-jar-with-dependencies.jar TicketClient

## Using Maven

mvn install

mvn package

java -jar target/iot-client-0.0.1-SNAPSHOT-jar-with-dependencies.jar

Note the plugin section in pom.xml to create a executabe jar
